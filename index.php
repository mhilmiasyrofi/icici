<html>
<head>
<TITLE>jQuery AJAX Autocomplete - Country Example</TITLE>
<head>
<style>

@import url(//fonts.googleapis.com/css?family=Open+Sans);
div {
    font-family: 'Open Sans', Calibri, Trebuchet, sans-serif;
  }
.frmSearch {margin: 2px 0px;padding:40px;border-radius:4px; text-align:center;}
.image img {
	margin-left: auto;
	margin-right: auto;
	display: block;
}
.valign-wrapper {
	text-align: center;
}
#presenter-list{margin:auto;list-style:none;padding:0;width:540px;}
#presenter-list li{padding: 10px; background: #F0F0F0; border-bottom: #bbb9b9 1px solid; text-align:left;}
#presenter-list li:hover{background:#ece3d2;cursor: pointer;}
#search-box{width: 540px; padding-left: 10px;border: #a8d4b1 1px solid;border-radius:4px; margin: 0 0 3px 0;}
tr {width: 20%;}
#berhasil, #tidak-berhasil {display: none; }
#berhasil div, #tidak-berhasil div {text-align: center;}
h5 {font-size: 200%;}
</style>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
<script>

var nama_peserta;

$(document).ready(function(){
	$("#search-box").keyup(function(){
		$.ajax({
		type: "POST",
		url: "readPresenter.php",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 600px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#search-box").css("background","#FFF");
		}
		});
	});
});

function selectPresenter(nama, role, status) {
	if (status == 1) {
		nama_peserta = nama;
		$("#nama-berhasil").text(nama);
		$("#role").text(role);
		$("#tidak-berhasil").hide();
		$("input:hidden").val(nama_peserta);
		$("#berhasil").show();
	} else {
		$("#nama-tidak-berhasil").text(nama);
		$("#berhasil").hide();
		$("#tidak-berhasil").show();
	}
	$("#search-box").val(nama);
	$("#suggesstion-box").hide();
}
</script>
</head>
<body>
	<div class="container">
        <!-- Page Content goes here -->
		</br></br></br></br></br></br>
		<div class="image">
			<img src="icici.png" alt="ICICI-BME 2017" class="responsive-image">
		</div>
		<div class="frmSearch">
			<input type="text" id="search-box" placeholder="Your Name" />
			<div id="suggesstion-box"></div>
		</div>
		</br>
		<div id="berhasil">
			<h5><div>Congratulation <div id="nama-berhasil" style="display: inline;">-</div></div></h5>
			<h5><div>Welcome to ICICI BME 2017</div></h5>
			<div>You are registered as <div id="role" style="display: inline; font-weight: bold;">-</div></div>
			</br>
			<form action="updatePresenter.php" method="POST">
				<input type='hidden' name='username'/>
				<div class="center-align">
				<button class="btn waves-effect waves-light" type="submit" name="action">Submit
					<i class="material-icons right">send</i>
				</button>
			</div>
			</form>
        </div>
		<div id="tidak-berhasil">
			<h5><div>Please Complete Your Registration <div id="nama-tidak-berhasil" style="display: inline;">-</div></div></h5>
        </div>

    </div>
</body>
</html>